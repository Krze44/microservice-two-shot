import { NavLink, Outlet } from 'react-router-dom';

function Nav() {
  const activeLinkStyle = { backgroundColor: '#c1c1c1' };
  
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark navbar-light">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">Wardrobify</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-link text-white" aria-current="page" to="/">Home</NavLink>
              </li>
              <li className="nav-item dropdown">
                <NavLink className="nav-link dropdown-toggle text-white" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" activeclassname ="active">
                  Shoes
                </NavLink>
                <ul className='dropdown-menu'>
                  <li><NavLink className="dropdown-item text-black" activestyle={activeLinkStyle} to="/shoes/list" activeclassname ="active">Shoes List</NavLink></li>
                  <li><NavLink className="dropdown-item text-black" activestyle={activeLinkStyle} to="/shoes/new" activeclassname ="active">Create a Shoe</NavLink></li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <NavLink className="nav-link dropdown-toggle text-white" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" activeclassname  ="active">
                  Hats
                </NavLink>
                <ul className='dropdown-menu'>
                  <li><NavLink className="nav-link text-black" activestyle={activeLinkStyle} to="/hats/list" activeclassname  ="active">Hats</NavLink></li>
                  <li><NavLink className="nav-link text-black" activestyle={activeLinkStyle} to="/hats/new" activeclassname ="active">Create a Hat</NavLink></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <Outlet />
    </>
  )
}

export default Nav;
