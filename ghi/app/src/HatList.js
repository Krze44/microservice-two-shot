import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';


function HatColumn(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                console.log(data);
                return (
                    <div key={hat.id} className="card mb-5 shadow rounded me-4">
                        <div className="d-flex justify-content-evenly card-title text-center mt-3">
                            <Link to="/hats/new" className="">
                                <button type="button" className="btn btn-outline-success">Create</button>
                            </Link>
                            <span className="card-text">{hat.style_name}</span>
                                <button type="button" className="btn btn-outline-danger " onClick={()=>props.onDelete(hat.id)}>Delete</button>
                        </div>
                        <div className="card-body">
                            <img src={hat.image} className="card-img-top" />
                            <p className="card-text">
                                Fabric:{hat.fabric}
                            </p>
                            <p className="card-text">
                                Color:{hat.color}
                            </p>
                            <p className="card-text">
                                Section Number:{hat.location.section_number}
                            </p>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

function HatList(props) {
    const [hatColumns, setHatColumns] = useState([[], [], []]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/locations/hats/";

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                // console.log(data);
                const columns = [[], [], []];
                let i = 0;
                for (let hat of data.hats) {
                    
                    columns[i].push(hat)
                    i = i + 1;
                    if (i > 2) {
                        i = 0;
                    }
                }
                setHatColumns(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }


    const onDelete = async(hatID) => {
        const hatURL = `http://localhost:8090/api/locations/hats/${hatID}/`;
        const response= await fetch(hatURL,
        {
            method:"DELETE",
            headers:{
                'Content-Type':'application/json'
            }
        });
        if (response.status>=200 && response.status<300){
            fetchData();
            document.location.reload();
        }
    }


    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="container text-center">
            <h2 className="mb-5">Your Hats</h2>
            <div className="row">
                {hatColumns.map((hatList, index) => {
                    return (
                        <HatColumn key={index} list={hatList} onDelete={onDelete}  />
                    );
                })}
            </div>
        </div>
    )
}




export default HatList


// import React, { useState, useEffect } from "react";

// export default function HatsList() {
//   const [hats, setHats] = useState([]);

//   async function loadHats() {
//     const response = await fetch("http://localhost:8090/api/hats/");
//     if (response.ok) {
//       const data = await response.json();
//       setHats(data.hats);
//     }
//   }

//   const handleDeleteHat = async (id) => {
//   const hatURL = `http://localhost:8090/api/hats/${id}`;
//   const response = await fetch(hatURL, { method: "DELETE"});
//   if (response.ok) {
//     setHats(hats.filter((hat) => hat.id !== id));
    
//   }
// }
//   useEffect(() => {
//     loadHats();
//   }, []);

//   return (
//     <div class="card" style={{ width: "18rem" }}>
//       {hats.map((hat) => (
//         <div key={hat.id}>
//           <img src={hat.picture} className="card-img-top" alt="Hat" />
//           <div class="card-body">
//             <h5 class="card-title">{hat.style}</h5>
//           </div>
//           <ul class="list-group list-group-flush">
//             <li class="list-group-item">
//               <strong>Fabric:</strong> {hat.fabric}
//             </li>
//             <li class="list-group-item">
//               <strong>Color:</strong> {hat.color}
//             </li>
//           </ul>
//           <button onClick={() => handleDeleteHat(hat.id)} className="btn btn-lg btn-primary" >Delete Hat </button> 
//         </div>
//       ))}
//     </div>
//   );
// }
