import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';



function ShoeColumn(props) {
    
    return (
        <div className="col">
            {props.list.map(data => {
                const shoe = data;
                console.log(data);
                return (
                    <div key={shoe.id} className="card mb-5 shadow rounded me-4">
                        <div className="d-flex justify-content-evenly card-title text-center mt-3">
                            <Link to="/shoes/new" className="">
                                <button type="button" className="btn btn-outline-success">Create</button>
                            </Link>
                            <span className="card-text ">{shoe.model_name}</span>
                            <button type="button" className="btn btn-outline-danger " onClick={() => props.onDelete(shoe.id)}> Delete</button>
                        </div>
                        <div className="card-body">
                            <img src={shoe.picture || 'https://peoplechange.nl/wp-content/uploads/2017/04/placeholder.jpg'} className="card-img-top" />
                            <p className="card-text">
                                {shoe.manufacturer}
                            </p>
                            <p className="card-text">
                                {shoe.color}
                            </p>
                            <p className="card-text">
                                {shoe.bin.bin_number}
                            </p>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

function ShoeList(props) {
    const [shoeColumns, setShoeColumns] = useState([[], [], []]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/bins/shoes/";

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                // console.log(data);
                const columns = [[], [], []];
                let i = 0;
                for (let shoe of data.shoes) {
                    
                    columns[i].push(shoe)
                    i = i + 1;
                    if (i > 2) {
                        i = 0;
                    }
                }
                setShoeColumns(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }

    const onDelete = async (shoeID) => {
        const response = await fetch(`http://localhost:8080/api/bins/shoes/${shoeID}/`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (response.status >= 200 && response.status < 300) {
            fetchData();
        }
    }
    
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container text-center">
            <h2 className="mb-5">Your Shoes</h2>
            <div className="row">
                {shoeColumns.map((shoeList, index) => {
                    return (
                        <ShoeColumn key={index} list={shoeList} onDelete={onDelete}/>
                    );
                })}
            </div>
        </div>
    )
}




export default ShoeList
