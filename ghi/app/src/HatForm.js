import React, { useEffect, useState } from "react";

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [image, setImage] = useState('');
    const [location, setLocation] = useState('');

    const handleSubmit= async (event) =>{
        event.preventDefault();
        const data={};

        data.style_name = styleName; 
        data.color = color;
        data.fabric = fabric;
        data.image = image;
        data.location=location
        console.log(data);

        const hatUrl = 'http://localhost:8090/api/locations/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setStyleName('');
            setColor('');
            setFabric('');
            setImage('');
            setLocation('');
        }
    }
    

    const handleStyeleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleImageChange = (event) => {
        const value = event.target.value;
        setImage(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }



    useEffect(()=>{
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/locations/';
            const response = await fetch(url);
            

            if (response.ok){
                const data = await response.json();
                setLocations(data.locations)


            }
        }
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleStyeleNameChange} placeholder="styleName" required type="text" name="styleName"  value={styleName} id="styleName" className="form-control"/>
                    <label htmlFor="name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} placeholder="color" required type="text" name="color"  value={color} id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} placeholder="fabric" required type="text" name="fabric"  value={fabric} id="fabric" className="form-control"/>
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleImageChange} placeholder="image" required type="text" name="image"  value={image} id="image" className="form-control"/>
                    <label htmlFor="image">Image</label>
                </div>
                <div className="mb-3">
                    <select onChange= {handleLocationChange} required name="location" value={location}  id="location" className="form-select">
                        <option value="">Choose a Location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.href} value={location.href}>
                                    {location.section_number}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default HatForm;
