import { NavLink } from "react-router-dom";
function MainPage() {
  return (
    <div className="container  text-center">
      <h1 className="display-5 fw-bold mb-5">WARDROBIFY!</h1>
      <h2 className="display-7 fw-bold mb-5 ">What would you like to view</h2>
      <div className="row">
        <div className="col me-5">
            <NavLink to="/shoes/list" className="text-black m-auto" style={{width: "450px"}}>
              <div className="card mb-3 shadow">
                <h5 className="card-title">Shoes</h5>
                <div className="card-body">
                  <img src="https://media.istockphoto.com/id/492339961/photo/different-shoes-displayed-in-a-shoe-shop.jpg?s=1024x1024&w=is&k=20&c=hIzXc3G4sO35SOCriv_Z4cGpP7T50d5APylaKxkrk6o=" alt="" className="card-img-top" />
                </div>
              </div>
            </NavLink>
        </div>
        <div className="col ">
            <NavLink to="/hats/list" className="text-black" style={{width: "450px"}}>
              <div className="card mb-3 shadow">
                <h5 className="card-title">Hats</h5>
                <div className="card-body">
                  <img src="https://blog.lids.com/wp-content/uploads/2018/09/Outside-the-US-3.jpg" alt="" className="card-img-top" />
                </div>
              </div>
            </NavLink>
        </div>
      </div>
    </div>
  );
}


export default MainPage;
