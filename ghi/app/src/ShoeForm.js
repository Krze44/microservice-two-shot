import React, { useEffect, useState } from "react";

function ShoesForm() {
    const [manufacturer, setManufacturer] = useState([]);
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [bins, setBins] = useState([]);
    const [selectedBins, setSelectedBins] = useState('');
    const [picture, setPicture] = useState();

    const updateInputState = function ({ target }, cb) {
    const { value } = target;

    cb(value);
    };

    const handleSubmit = async function (event) {
    event.preventDefault();
    const data = {
        manufacturer,
        model_name,
        color,
        picture,
        bin: selectedBins,
    };

    const shoesUrl = "http://localhost:8080/api/bins/shoes/";
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
    };

    try {
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();
            console.log("Shoe created:", newBin);
            event.target.reset()
        } else {
            throw new Error("Network response was not ok");
        }
        } catch (error) {
        console.error("Error adding bin:", error);
        }
    };

    const fetchBins = async () => {
        const binsUrl = "http://localhost:8100/api/bins/";
        const response = await fetch(binsUrl);
        if (response.ok) {
            const data = await response.json();
            const sortedBins = data.bins.sort((a, b) => a - b);
            setBins(sortedBins);
        } else {
            console.error("Error fetching bins:", response.statusText);
        }
    };

    useEffect(() => {
    fetchBins();
    }, []);



    return (
    <>
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a new shoe to your collection</h1>
            <form id="create-shoe-form" onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                <input
                    placeholder=""
                    required
                    type="text"
                    name="manufacturer"
                    id="manufacturer"
                    className="form-control"
                    onChange={(event) => updateInputState(event, setModelName)}
                />
                <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    placeholder=""
                    required
                    type="text"
                    name="model_name"
                    id="model_name"
                    className="form-control"
                    onChange={(event) => updateInputState(event, setManufacturer)}
                />
                <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    placeholder=""
                    required
                    type="text"
                    name="color"
                    id="color"
                    className="form-control"
                    onChange={(event) => updateInputState(event, setColor)}
                />
                <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    placeholder=""
                    required
                    type="url"
                    name="picture"
                    id="picture"
                    className="form-control"
                    onChange={(event) => updateInputState(event, setPicture)}
                />
                <label htmlFor="color">Shoe Image URL</label>
                </div>
                <div className="mb-3">
                    <select
                        required
                        name="bin"
                        id="bin"
                        className="form-select"
                        value={selectedBins}
                        onChange={(event) => setSelectedBins(event.target.value)}
                        >
                        <option value="">Choose a bin</option>
                        {bins.map((bin) => (
                        <option key={bin.id} value={bin.href}>
                            {bin.bin_number}
                        </option>
                        ))}
                    </select>
                </div>

                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    </>
);
}

export default ShoesForm;
