import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';
import HatList from './HatList';
import HatForm from './HatForm';
import Nav from './Nav';


function App(props) {
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="hats"> 
                        <Route path="list"  element={<HatList  hats={props.hats}/>}  />
                        <Route path="new" element={<HatForm />}  />
                    </Route>
                    <Route path="shoes"> 
                        <Route path="list" element={<ShoeList shoes={props.shoes}/>} />
                        <Route path="new" element={<ShoeForm />}   />
                    </Route>
                </Routes>
            </div>
        </BrowserRouter>
    );
}
export default App;
