from django.shortcuts import render
from .models import Hat,LocationVO
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse


# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "section_number"]
    

class HatListEncoder(ModelEncoder):
    model= Hat
    properties = [
        "style_name",
        "fabric",
        "color",
        "image",
        "id",
        "location",
        ]

    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):

    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)


        try:
            location_href=content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location      
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_hats(request, pk):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        pass
