from django.db import models
from django.urls import reverse
# Create your models here.




class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    bin_number = models.SmallIntegerField(unique=True)
    bin_size = models.SmallIntegerField()
    closet_name = models.CharField(max_length=200)


class ShoeModel(models.Model):
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField()
    manufacturer = models.CharField(max_length=100)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.CASCADE,
        
    )

    def __str__(self):
        return self.model_name

    class Meta:
        ordering = ("model_name",)



