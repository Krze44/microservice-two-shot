from django.urls import path
from .views import api_list_shoes, api_delete_shoes

urlpatterns = [
    path("bins/shoes/", api_list_shoes, name="api_list_shoes"),
    path("bins/shoes/<int:pk>/", api_delete_shoes, name="api_delete_shoes"),
]
